# spatial-ecology-accelerate

## Installation: Haskell & Accelerate

 1. Download the Haskell build tool [`stack`](https://docs.haskellstack.org/en/stable/README/)

 2. Install LLVM-8, this is a dependency of Accelerate

    1. **macOS:** This can be obtained from via Homebrew:

    ```sh
    brew install llvm-hs/llvm/llvm-8
    ```

    2. **debian/ubuntu:** Available via apt:

    ```sh
    apt-get install llvm-8
    ```

 3. Finally, enter the following command:

    ```sh
    stack build
    ```

    This will download and build everything. It will take a while; go get a
    coffee and/or delicious pastry while you wait.

## Installation: IHaskell/Jupyter (optional)

  1. Install Jupyter. The recommended method is to use the [Anaconda
     Distribution](https://www.anaconda.com/distribution/), which includes
     Python and several commonly used packages. Make sure this version of
     `python3` is the first one on your `PATH`.

  2. You _may_ need to install a few extra dependencies (for example):

     1. **macOS:**

     ```sh
     brew install zeromq cairo pango
     ```

     2. **debian/ubuntu:***

     ```sh
     apt-get install libzmq3-dev libcairo2-dev libpango1.0-dev
     ```

  3. Build the `ihaskell` tool

     ```sh
     stack install ihaskell
     ```

  4. Install IHaskell into Jupyter

     ```sh
     ihaskell install --stack
     ```

     You can now open Haskell-based notebooks through the usual `jupyter
     notebook` command.

## Running

TODO

