//
//  HydroFunctions.cl
//
//  Created by Johan Van de Koppel on 31/08/2017.
//  Copyright 2017 Johan Van de Koppel. All rights reserved.
//

#ifndef HYDROFUNCTIONS_CL
#define HYDROFUNCTIONS_CL

////////////////////////////////////////////////////////////////////////////////
// Apriori prototyping
////////////////////////////////////////////////////////////////////////////////

float Netspeed_v(__global float*, __global float*);
float Netspeed_u(__global float*, __global float*);
float Netspeed_a(__global float*, __global float*);

float Depth_v(__global float*);
float Depth_u(__global float*);

float Friction_v(__global float*, __global float*);
float Friction_u(__global float*, __global float*);

float d2u_dxy2(__global float*, float, float);
float d2v_dxy2(__global float*, float, float);

float d2s_dxy2(__global float*,float,float);
float d2p_dxy2(__global float*,float,float);

float dO_dx(__global float*, __global float*, __global float*, float,float);
float dO_dy(__global float*, __global float*, __global float*, float,float);

float dau2_dx(__global float* h, __global float*, float dx2, float dy2);
float dauv_dy(__global float* h, __global float*, __global float*,float,float);

float dauv_dx(__global float* h, __global float*, __global float*,float,float);
float dav2_dy(__global float* h, __global float*,float, float);

float d_uh_dx(__global float* u, __global float*, float, float);
float d_vh_dy(__global float* v, __global float*, float, float);

typedef unsigned int Thread_Id;
#define ON              1
#define OFF             0


////////////////////////////////////////////////////////////////////////////////
// Averaging Operations
////////////////////////////////////////////////////////////////////////////////

float Netspeed_v(__global float* u, __global float* v)
{
    // Getting thread coordinates on the grid
    const size_t current    = get_global_id(0);
    const size_t row_v      = (size_t)current/Grid_Width_v;
    const size_t column_v   = current%Grid_Width_v;
    
    float u_v, v_v;
    
    // ---- Internal grid points ------
    u_v = (u[current-1] + u[current] + u[current-1-Grid_Width] + u[current-Grid_Width])/4.0;
    v_v = v[current];
    
    // ---- Boundary grid points ------
    
    // Bottom Boundary
    if(UDirichletBottom == 1 && row_v == 0 && column_v > 0){
        u_v = UValueBottom;
    } else if (row_v ==0 && column_v > 0){
        u_v = (u[current-1] + u[current])/2.0;
    }
    
    // Top Boundary
    if(UDirichletTop == 1 && row_v == Grid_Height_v - 1 && column_v > 0){
        u_v = UValueTop;
    } else if (row_v == Grid_Height_v-1 && column_v > 0){
        u_v = (u[current-1-Grid_Width] + u[current-Grid_Width])/2.0;
    }
    
    // Left Boundary
    if(UDirichletLeft == 1 && column_v == 0 && row_v > 0 && row_v < Grid_Height_v-1){
        u_v = UValueLeft;
    } else if (column_v == 0 && row_v > 0 && row_v < Grid_Height_v-1){
        u_v = (u[current] + u[current-Grid_Width])/2.0;
    }
    
    // Right boundary: no problem, because v is not on that boundary
    
    // Bottom Left Vertex
    if(UDirichletBottom == 1 && row_v == 0 && column_v == 0){
        u_v = UValueBottom;
    }
    else if(UDirichletLeft == 1 && row_v ==0 && column_v == 0){
        u_v = UValueLeft;
    } else if (row_v == 0 && column_v == 0){
        u_v = u[current];
    }
    
    // Top Left Vertex
    if(UDirichletTop == 1 && row_v == Grid_Height_v-1 && column_v == 0){
        u_v = UValueTop;
    }
    else if(UDirichletLeft == 1 && row_v == Grid_Height_v-1 && column_v == 0){
        u_v = UValueLeft;
    } else if (row_v == Grid_Height_v-1 && column_v == 0){
        u_v = u[current-Grid_Width];
    }
    
    return (sqrt(u_v*u_v+v_v*v_v));
}

float Netspeed_u(__global float* u, __global float* v)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    float u_u, v_u;
    
    // ---- Internal grid points -----
    u_u = u[current];
    v_u = (v[current] + v[current+1] + v[current + Grid_Width] + v[current + Grid_Width+1])/4.0;
    
    // ---- Boundary grid points ------
    
    // Right Boundary
    if(VDirichletRight ==1 && column == Grid_Width-1){
        v_u = VValueRight;
    } else if (column == Grid_Width-1){
        v_u = (v[current] +  v[current + Grid_Width])/2.0;;
    }
    
    return (sqrt(u_u*u_u + v_u*v_u));
}

float Netspeed_a(__global float* u, __global float* v)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width_v;
    const size_t column     = current%Grid_Width_v;
    
    float u_a, v_a;
    
    // --- Internal grid points ------
    u_a = (u[current-1]+u[current])/2.0;
    v_a = (v[current] + v[current+Grid_Width])/2.0;
    
    // ---- Boundary grid points -----
    
    //Left boundary
    if(UDirichletLeft == 1 && column == 0){
        u_a = UValueLeft;
    }else if(column == 0){
        u_a = u[current];
    }
    
    return (sqrt(u_a*u_a + v_a*v_a));
}

float Depth_v(__global float* h)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v = (size_t)current/Grid_Width_v;
    const size_t column_v  = current%Grid_Width_v;
    
    float h_v;
    
    // ---- Internal grid points --------
    h_v = ((h[current] + h[current-Grid_Width_v])/2.0);
    
    // ---- Boundary grid points --------
    
    // Bottom boundary
    if(ADirichletBottom == 1 && row_v == 0){
        h_v = AValueBottom;
    }else if (row_v ==0){
        h_v = h[current];
    }
    
    // Top boundary
    if(ADirichletTop == 1 && row_v ==Grid_Height_v-1){
        h_v = AValueTop;
    }else if (row_v == Grid_Height_v-1){
        h_v = h[current-Grid_Width_v];
    }
    
    return h_v;
}

float Depth_u(__global float* h)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    float h_u;
    
    // ---- Internal grid points --------
    h_u = ((h[current] + h[current+1])/2.0);
    
    // ---- Boundary grid points --------
    
    // Right boundary
    if (ADirichletRight == 1 && column == Grid_Width-1){
        h_u = AValueRight;
    } else if (column == Grid_Width-1){
        h_u = h[current];
    }
    
    return h_u;
}

float Friction_v(__global float* h, __global float* p)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v     = (size_t)current/Grid_Width_v;
    const size_t column_v     = current%Grid_Width_v;
    
    float C_t, h_v, Dv;
    
    // ---- Internal grid points --------
    float Dv_1       = 1.0*p[current];
    float Dv_2       = 1.0*p[current-Grid_Width_v];
    
    float h_1 = h[current];
    float h_2 = h[current-Grid_Width_v];
    
    float Ct_1       = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv_1*Hv)) )
                       + sqrt((float)(g/kv)*log((float)cl_max(h_1/Hv,1.0)) );
    float Ct_2      = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv_2*Hv)) )
                       + sqrt((float)(g/kv)*log((float)cl_max(h_2/Hv,1.0)) );
    
    C_t = ((Ct_1+Ct_2)/2.0);
    
    // ---- Boundary grid points --------
    
    // Bottom boundary
    if(ADirichletBottom == 1 && row_v == 0){
        h_v     = AValueBottom;
        Dv      = 1.0*p[current];
        C_t     = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                  + sqrt((float)(g/kv)*log(cl_max(h_v/Hv,1.0)) );
    }else if (row_v == 0){
        h_v     = h[current];
        Dv      = 1.0*p[current];
        C_t     = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                  + sqrt((float)(g/kv)*log(cl_max(h_v/Hv,1.0)) );
    }
    
    // Top boundary
    if(ADirichletTop == 1 && row_v ==Grid_Height_v-1){
        h_v     = AValueTop;
        Dv      = 1.0*p[current-Grid_Width_v];
        C_t     = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                  + sqrt((float)(g/kv)*log(cl_max(h_v/Hv,1.0)) );
    }else if (row_v ==Grid_Height_v-1){
        h_v     = h[current-Grid_Width_v];
        Dv      = 1.0*p[current-Grid_Width_v];
        C_t     = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                  + sqrt((float)(g/kv)*log(cl_max(h_v/Hv,1.0)) );
    }
    
    return C_t;
}

float Friction_u(__global float* h, __global float* p)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    float Ct, Ct_1, Ct_2, h_u, Dv;
    
    float Dv_1       = 1.0*p[current];
    float Dv_2       = 1.0*p[current+1];
    
    // ---- Internal grid points --------
    Ct_1       = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv_1*Hv)) )
                + sqrt((float)(g/kv)*log(cl_max(h[current]/Hv,1.0)) );
    Ct_2      = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv_2*Hv)) )
                + sqrt((float)(g/kv)*log(cl_max(h[current+1]/Hv,1.0)) );
    
    Ct = ((Ct_1+Ct_2)/2.0);
    
    // ---- Boundary grid points --------
    // Right boundary
    if (ADirichletRight == 1 && column == Grid_Width-1){
        h_u     = AValueRight;
        Dv      = 1.0*p[current];
        Ct      = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                 + sqrt((float)(g/kv)*log(cl_max(h_u/Hv,1.0)) );
    } else if (column == Grid_Width-1){
        h_u     = h[current];
        Dv      = 1.0*p[current];
        Ct      = sqrt((float)(1.0/(1.0/(Cb*Cb)+1.0/(2.0*g)*Cd*Dv*Hv)) )
                 + sqrt((float)(g/kv)*log(cl_max(h_u/Hv,1.0)) );
    }
    
    return Ct;
}



////////////////////////////////////////////////////////////////////////////////
// Laplacation operator definition, to calculate diffusive terms
////////////////////////////////////////////////////////////////////////////////

float d2u_dxy2(__global float* pop, float dx2, float dy2)
{
    const float dx = dX;
    const float dy = dY;
    float du_dx_l, du_dx_r, du_dy_t, du_dy_b, ret;
    
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column  = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    const size_t top     = (row+1) * Grid_Width + column;
    const size_t bottom  = (row-1) * Grid_Width + column;
    
    //---- Internal nodes -----------
    du_dx_r = pop[right] - pop[current];
    du_dx_l = pop[current] - pop[left];
    du_dy_t = pop[top] - pop[current];
    du_dy_b = pop[current] - pop[bottom];
    
    
    //---- Boundary nodes ------------
    
    //Left boundary
    if(UNeumannLeft == 1 && column == 0){
        du_dx_l = 0.0;
    }
    else if(UDirichletLeft == 1 && column == 0){
        du_dx_l = 2*pop[current] - 2*UValueLeft;                        // Ghost point = 2*Value - pop[current];
    }
    
    // Right boundary
    if(UNeumannRight == 1 && column == Grid_Width-1){
        du_dx_r= 0.0;
    }
    
    // Top boundary
    if(UNeumannTop == 1 && row == Grid_Height-1){
        du_dy_t = 0.0;
    }
    else if(UDirichletTop == 1 && row == Grid_Height-1){
        du_dy_t = 2*UValueTop - 2*pop[current] ;
    }
    
    
    // Bottom boundary
    if(UNeumannBottom == 1 && row == 0){
        du_dy_b = 0.0;
    }
    else if(UDirichletBottom == 1 && row == 0){
        du_dy_b =  2*pop[current] - 2*UValueBottom;
    }
    
    
    //--- Return value -----
    ret = (dy2*(du_dx_r - du_dx_l)/dx) + (dx2*(du_dy_t - du_dy_b)/dy);
    return ret;
}


float d2v_dxy2(__global float* pop,float dx2,float dy2)
{
    const float dx = dX;
    const float dy = dY;
    
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v     = (size_t)current/Grid_Width_v;
    const size_t column_v  = current%Grid_Width_v;
    
    
    // Neighbouring grid points
    const size_t left    = row_v * Grid_Width_v + column_v-1;
    const size_t right   = row_v * Grid_Width_v + column_v+1;
    const size_t top     = (row_v+1) * Grid_Width_v + column_v;
    const size_t bottom  = (row_v-1) * Grid_Width_v + column_v;
    
    float dv_dx_l, dv_dx_r, dv_dy_t, dv_dy_b, ret;
    
    //---- Internal nodes -----
    dv_dx_r = pop[right] - pop[current];
    dv_dx_l = pop[current] - pop[left];
    dv_dy_t = pop[top] - pop[current];
    dv_dy_b = pop[current] - pop[bottom];
    
    
    //---- Boundary nodes ------
    
    //Left Boundary
    if(VNeumannLeft == 1 && column_v == 0){
        dv_dx_l= 0.0;
    }
    
    // Right boundary
    if(VNeumannRight == 1 && column_v == Grid_Width_v-1){    //Of dirichlet condition of Neumann condition
        dv_dx_r = 0.0;
    }
    else if(VDirichletRight == 1 && column_v == Grid_Width_v-1){
        dv_dx_r = 2*VValueRight - 2*pop[current] ;
    }
    
    //Bottom boundary
    if(VNeumannBottom == 1 && row_v == 0){
        dv_dy_b= 0.0;
    }
    
    //Top boundary
    if(VNeumannTop == 1 && row_v == Grid_Height_v-1){
        dv_dy_t= 0.0;
    }
    
     //--- Return value -----
    ret = (dy2*(dv_dx_r - dv_dx_l)/dx) + (dx2*(dv_dy_t - dv_dy_b)/dy);
    return ret;
}


float d2s_dxy2(__global float* pop,float dx2,float dy2)
{
    const float dx = dX;
    const float dy = dY;
    
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column  = current%Grid_Width;
    
    
    // Neighbouring grid points
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    const size_t top     = (row+1) * Grid_Width + column;
    const size_t bottom  = (row-1) * Grid_Width + column;
    
    float ds_dx_l, ds_dx_r, ds_dy_t, ds_dy_b, ret;
    
    //----- Internal nodes ------
    ds_dx_r = pop[right] - pop[current];
    ds_dx_l = pop[current] - pop[left];
    ds_dy_t = pop[top] - pop[current];
    ds_dy_b = pop[current] - pop[bottom];
    
    
    //---- Boundary nodes -------
    
    //Left Boundary
    if(SNeumannLeft == 1 && column == 0){
        ds_dx_l = 0.0;
    }

    
    // Right boundary
    if(SNeumannRight == 1 && column == Grid_Width-1){
        ds_dx_r= 0.0;
    }else if (SDirichletTop == 1 &&  column == Grid_Width-1){
        ds_dx_r = 2*SValueLeft - 2*pop[current] ;
    }
        
    
    //Top boundary
    if(SNeumannTop == 1 && row == Grid_Height-1){
        ds_dy_t = 0.0;
    }
    else if(SDirichletTop == 1 && row == Grid_Height-1){
        ds_dy_t = 2*SValueTop - 2*pop[current] ;
    }
    
    //Bottom boundary
    if(SNeumannBottom == 1 && row == 0){
        ds_dy_b = 0.0;
    }
    else if(SDirichletBottom == 1 && row == 0){
        ds_dy_b =  2*pop[current] - 2*SValueBottom;
    }
    
     //--- Return value -----
    ret = (dy2*(ds_dx_r - ds_dx_l)/dx) + (dx2*(ds_dy_t - ds_dy_b)/dy);
    return ret;
}


float d2p_dxy2(__global float* pop,float dx2,float dy2)
{
    const float dx = dX;
    const float dy = dY;
    
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column  = current%Grid_Width;
    
    
    // Neighbouring grid points
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    const size_t top     = (row+1) * Grid_Width + column;
    const size_t bottom  = (row-1) * Grid_Width + column;
    
    float dp_dx_l, dp_dx_r, dp_dy_t, dp_dy_b, ret;
    
    // ----- Internal nodes -----
    dp_dx_r = pop[right] - pop[current];
    dp_dx_l = pop[current] - pop[left];
    dp_dy_t = pop[top] - pop[current];
    dp_dy_b = pop[current] - pop[bottom];
    
    
    //---- Boundary nodes -----
    
    //Left boundary
    if(PNeumannLeft == 1 && column == 0){
        dp_dx_l = 0.0;
    }
    
    
    // Right boundary condition
    if(PNeumannRight == 1 && column == Grid_Width-1){
        dp_dx_r= 0.0;
    } else if (PDirichletRight == 1 &&  column == Grid_Width-1){
        dp_dx_r = 2*PValueLeft - 2*pop[current] ;
    }
    
    
    //Top boundary condition
    if(PNeumannTop == 1 && row == Grid_Height-1){
        dp_dy_t = 0.0;
    }
    else if(PDirichletTop == 1 && row == Grid_Height-1){
        dp_dy_t = 2*PValueTop - 2*pop[current] ;
    }
    
    //Bottom boundary condition
    if(PNeumannBottom == 1 && row == 0){
        dp_dy_b = 0.0;
    }
    else if(PDirichletBottom == 1 && row == 0){
        dp_dy_b =  2*pop[current] - 2*PValueBottom;
    }
    
     //--- Return value -----
    ret = (dy2*(dp_dx_r - dp_dx_l)/dx) + (dx2*(dp_dy_t - dp_dy_b)/dy);
    return ret;
}


////////////////////////////////////////////////////////////////////////////////
// Gradient operator definitions, to calculate the pressure gradient
////////////////////////////////////////////////////////////////////////////////

float dO_dx(__global float* h, __global float* s, __global float* b, float dx2,float dy2)
{

    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t right   = row * Grid_Width + column+1;
    float O_r, O_l, ret;
    
    // ---- Internal grid points------
    O_r = h[right]+s[right]+b[right];
    O_l = h[current]+s[current]+b[current];
    
    //--- Boundary grid points -------
    
    // Right boundary                                                            - Dirichlet, does not enter this function
    if(ADirichletRight == 1 && column == Grid_Width-1){
        O_r = AValueRight+s[current]+slope*LengthX;
    }else if(column == Grid_Width-1){
        O_r = h[current]+s[current]+slope*LengthX;
    }
    
     //--- Return value -----
    ret = dy2*(O_r - O_l);
    return  ret;
}

float dO_dy(__global float* h, __global float* s, __global float* b, float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v     = (size_t)current/Grid_Width_v;
    const size_t column_v     = current%Grid_Width_v;
    
    // Neighbouring grid points
    const size_t bottom  = (row_v-1) * Grid_Width_v + column_v;
    
    float O_t, O_b, ret;
    
    // ---- Internal nodes ------
    
    O_t = h[current] + s[current]+ b[current];
    O_b = h[bottom] + s[bottom]+ b[bottom];
    
    
    // ---- Boundary nodes ------
    
    // Top boundary                                                   - Dirichlet, does not enter this function
    if(ADirichletTop == 1 && row_v == Grid_Height_v-1){
        O_t = AValueTop+s[bottom]+b[bottom];
    }else if (row_v == Grid_Height_v-1){
        O_t = h[bottom]+s[bottom]+b[bottom];
    }
    
    // Bottom boundary                                                    - Dirichlet, does not enter this function
    if(ADirichletBottom == 1 && row_v == 0){
        O_b = AValueBottom+s[current]+b[current];
    }else if (row_v == 0){
        O_b = h[current]+s[current]+b[current];
    }
    
    ret = (dx2*(O_t - O_b));
   
   return ret;
    
}


////////////////////////////////////////////////////////////////////////////////
// FVM - derivative convective terms
////////////////////////////////////////////////////////////////////////////////

float dau2_dx(__global float* h, __global float* u, float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    
    float u2_l_h_l, u2_r_h_r;
    float ret;
    
    // ---- Internal nodes ------
    
    u2_r_h_r = h[right]* (u[right]*u[right] +u[current]*u[current])/2.0;
    u2_l_h_l = h[current]*(u[left]*u[left]+u[current]*u[current])/2.0;
    
    
    //--- Boundary nodes -------
    // Left boundary                                          - Dirichlet, does not enter this function
    if (UDirichletLeft == 1 && column == 0){
        u2_l_h_l = h[current]*UValueLeft*UValueLeft;
    }
    else if (column == 0){
        u2_l_h_l = h[current]*u[current]*u[current];
    }
    
    // Right boundary                                        - Dirichlet, does not enter this function
    if(ADirichletRight == 1 && column == Grid_Width-1){
        u2_r_h_r = AValueRight*u[current]*u[current];
    }else if(column == Grid_Width-1){
        u2_r_h_r = h[current]*u[current]*u[current];
    }
    
    //Return value
    return (dy2*(u2_r_h_r - u2_l_h_l));
    
}


float dauv_dy(__global float* h, __global float* u, __global float* v,float dx2, float dy2)
{

    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t top     = (row+1) * Grid_Width + column;
    const size_t bottom  = (row-1) * Grid_Width + column;
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    
    float u_t_v_t_h_t, u_b_v_b_h_b;
    float h_t, u_t, v_t, h_b, u_b, v_b;
    float ret;
    
    // ---- Internal node -------
    h_t = (h[current]+h[right] + h[top]+ h[top+1])/4.0;
    u_t = (u[current]+u[top])/2.0;
    v_t = (v[top] + v[top+1])/2.0;
    
    
    h_b = (h[current]+h[right] + h[bottom]+ h[bottom+1])/4.0;
    u_b = (u[current]+u[bottom])/2.0;
    v_b = (v[current] + v[right])/2.0;
    
    //--- Boundary nodes -------
    // Bottom boundary
    ////u
    if(UDirichletBottom == 1 && row == 0){
        u_b = UValueBottom;
    }else if(row == 0){
        u_b = u[current];
    }
    
    ////h
    if(ADirichletBottom == 1 && row == 0){
        h_b = AValueBottom;
    } else if (row == 0){
        h_b = (h[current] + h[right])/2.0;
    }
    
    // Top boundary condition
    ////u
    if(UDirichletTop == 1 && row == Grid_Height-1){
        u_t = UValueTop;
    }else if(row == Grid_Height-1){
        u_t = u[current];
    }
    
    if(ADirichletTop == 1 && row == Grid_Height-1){
        h_t = AValueTop;
    } else if (row == Grid_Height-1){
        h_t = (h[current] + h[right])/2.0;
    }
    
    // On the right boundary condition                                  - Dirichlet, does not enter this function
    //// v
    if(VDirichletRight == 1 && column == Grid_Width - 1){
        v_t = VValueRight;
        v_b = VValueRight;
    }else if(column == Grid_Width - 1){
        v_t = v[top];
        v_b = v[current];
    }
    
    ////h
    if(ADirichletRight == 1 && column == Grid_Width - 1){
        h_t = AValueRight;
        h_b = AValueRight;
    }else if(column == Grid_Width - 1){
        h_t = (h[top]+h[current])/2.0;
        h_b = (h[current]+h[bottom])/2.0;
    }
    
    u_t_v_t_h_t = u_t*v_t*h_t;
    u_b_v_b_h_b = u_b*v_b*h_b;
    
    
    //Return value
    return (dx2*(u_t_v_t_h_t
                 -u_b_v_b_h_b));
}


float dauv_dx(__global float* h, __global float* u, __global float* v,float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v     = (size_t)current/Grid_Width_v;
    const size_t column_v     = current%Grid_Width_v;
    
    // Neighbouring grid points
    const size_t top     = (row_v+1) * Grid_Width_v + column_v;
    const size_t bottom  = (row_v-1) * Grid_Width_v + column_v;
    const size_t left    = row_v * Grid_Width_v + column_v-1;
    const size_t right   = row_v * Grid_Width_v + column_v+1;
    
    float u_r_v_r_h_r, u_l_v_l_h_l;
    float h_r, u_r, v_r, h_l, u_l, v_l;
    float ret;
    
    h_r = (h[current] + h[right] + h[bottom]+h[bottom+1])/4.0;
    u_r = (u[current] + u[bottom])/2.0;
    v_r = (v[current] + v[right])/2.0;
    
    h_l = (h[current] + h[left]+ h[bottom]+h[bottom-1])/4.0;
    u_l = (u[left] + u[bottom-1])/2.0;
    v_l = (v[current]+v[left])/2.0;
    
    
    //--- Boundary Conditions -------
    // Left boundary condition                                          - u Dirichlet, does not enter this function
    if(UDirichletLeft == 1 && column_v == 0){
        h_l = (h[current] + h[bottom])/2.0;
        u_l = UValueLeft;
        v_l = v[current];

    } else if (column_v == 0){
        h_l = (h[current] + h[bottom])/2.0;
        u_l = u[current];
        v_l = v[current];
    }
    
    // Right boundary condition                                         - u Dirichlet, does not enter this function
    if(ADirichletRight == 1 && column_v == Grid_Width_v){
        h_r = AValueRight;
        v_r = v[current];
    } else if (column_v == Grid_Width_v){
        h_r = (h[current] + h[bottom])/2.0;
        v_r = v[current];
    }
    
    //Bottom boundary condition
    ////v


    //u
    if(UDirichletBottom == 1 && row_v ==0){
        u_r = UValueBottom;
        u_l = UValueBottom;
    }else if(row_v ==0){
        u_r = u[current];
        u_l = u[current-1];
    }
    
    //h
    if(ADirichletBottom == 1 && row_v ==0){
        h_r = AValueBottom;
        h_l = AValueBottom;
    }else if(row_v ==0){
        h_r = (h[current] + h[current+1])/2.0;
        h_l = (h[current] + h[current-1])/2.0;
    }
    
    
    //Top boundary condition
    //u
    if(UDirichletTop == 1 && row_v == Grid_Height_v-1){
        u_r = UValueTop;
        u_l = UValueTop;
    }else if(row_v == Grid_Height_v-1){
        u_r = u[current-Grid_Width_v];
        u_l = u[current-Grid_Width_v-1];
    }
    
    //h
    if(ADirichletBottom == 1 && row_v  == Grid_Height_v-1){
        h_r = AValueTop;
        h_l = AValueTop;
    }else if(row_v == Grid_Height_v-1){
        h_r = (h[current-Grid_Width_v] + h[current-Grid_Width_v+1])/2.0;
        h_l = (h[current-Grid_Width_v] + h[current-Grid_Width_v-1])/2.0;
    }
    

    // Return value
    return (dy2*(u_r*v_r*h_r -u_l*v_l*h_l));
    

}

float dav2_dy(__global float* h, __global float* v,float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row_v     = (size_t)current/Grid_Width_v;
    const size_t column_v     = current%Grid_Width_v;
    
    // Neighbouring grid points
    const size_t top     = (row_v+1) * Grid_Width_v + column_v;
    const size_t bottom  = (row_v-1) * Grid_Width_v + column_v;
    
    float v2_b_h_b, v2_t_h_t;
    float ret;
    
        // ---- Internal nodes ------
    v2_t_h_t = h[current]*(v[current]*v[current]+v[top]*v[top])/2.0;
    v2_b_h_b = h[bottom]*(v[current]*v[current]+v[bottom]*v[bottom])/2.0;
    
    //--- Boundary nodes -------
    
    // Top boundary                                               - v Dirichlet, does not enter this function
    if(ADirichletTop == 1 && row_v == Grid_Height_v-1){
        v2_t_h_t = AValueTop*v[current]*v[current];
    }else if(row_v == Grid_Height_v-1){
        v2_t_h_t = h[bottom]*v[current]*v[current];
    }
    
    // Bottom boundary
    if(ADirichletBottom == 1 && row_v == 0){
        v2_b_h_b = AValueBottom*v[current]*v[current];
    }else if(row_v == 0){
        v2_b_h_b = h[current]*v[current]*v[current];
    }
    
    //Return value
    ret = (dx2*(v2_t_h_t-v2_b_h_b));
    return ret;
}


////////////////////////////////////////////////////////////////////////////////
// Definition of the functions that compute the derivatives of uh and vh
////////////////////////////////////////////////////////////////////////////////

float d_uh_dx(__global float* u, __global float* h, float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t left    = row * Grid_Width + column-1;
    const size_t right   = row * Grid_Width + column+1;
    
    float u_r_h_r, u_l_h_l;
    float ret;
    
    // ---- Internal nodes ------
    u_r_h_r = u[current]*(h[right]+h[current])/2.0;
    u_l_h_l = u[left]*(h[left]+h[current])/2.0;
    
    //--- Boundary nodes -------
    //--- a Dirichlet (and on boundary), does not enter this function
    
    //Left Boundary         (a on boundary)
    if(UDirichletLeft == 1 && column == 0){
        u_l_h_l = h[current]*UValueLeft;
    }
    else if(column == 0){                                            // Homogenous Neumann condition u
        u_l_h_l =  u[current]*h[current];
    }
    
    //Right boundary        (u on boundary)
    if(ADirichletRight == 1 && column == Grid_Width-1){
        u_r_h_r = AValueRight * u[current];
    }else if(column == Grid_Width-1){                                // Homogenous Neumann condition a
        u_r_h_r = h[current]*u[current];
    }
    
    //Return value
    ret  = dy2*(u_r_h_r - u_l_h_l);
    return ret;
}

float d_vh_dy(__global float* v, __global float* h, float dx2, float dy2)
{
    // Getting thread coordinates on the grid
    const size_t current = get_global_id(0);
    const size_t row     = (size_t)current/Grid_Width;
    const size_t column     = current%Grid_Width;
    
    // Neighbouring grid points
    const size_t top     = (row+1) * Grid_Width + column;
    const size_t bottom  = (row-1) * Grid_Width + column;
    
    float v_t_h_t, v_b_h_b;
    float ret;
    
    // ---- Internal nodes ------
     v_t_h_t = v[top]*(h[top]+h[current])/2.0;
     v_b_h_b = v[current]*(h[bottom]+h[current])/2.0;
    
    
    //--- Boundary Conditions -------                                /// a Dirichlet and on boundary, does not enter this function
    
    //Left Boundary         (a on boundary)
    if(ADirichletBottom == 1 && row == 0){
        v_b_h_b = v[current]*AValueBottom;
    }
    else if(row == 0){                                              // homogenous Neumann condition voor u
        v_b_h_b =  v[current]*h[current];
    }
    
    //Right boundary        (u on boundary)
    if(ADirichletTop == 1 && row == Grid_Height-1){
        v_t_h_t = AValueTop * v[top];
    }else if(row == Grid_Height-1){                                 // Homogenous Neumann condition a
        v_t_h_t = h[current]* v[top];
    }
    
    ret  = (dx2*(v_t_h_t-v_b_h_b));
    
    return ret;
}




#endif


