# USAGE:
#   1. docker build -t spatial-ecology-accelerate .
#   2. uncomment the docker section from stack.yaml
#   3. stack install ihaskell
#   4. ihaskell install --stack
#   5. stack exec jupyter -- notebook --ip=0.0.0.0
#

# https://hub.docker.com/r/nvidia/cuda/
# FROM fpco/stack-build:lts-13.12
FROM nvidia/cuda:10.1-devel-ubuntu18.04
LABEL maintainer "Trevor L. McDonell <trevor.mcdonell@gmail.com>"

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PATH /root/.local/bin:${PATH}

RUN apt-get update \
 && apt-get install -y software-properties-common apt-utils curl netbase pkg-config wget libgmp-dev

# Install LLVM
RUN wget -O - http://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - \
 && add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-8 main" \
 && apt-get update \
 && apt-get install -y llvm-8-dev

# Install necessary IHaskell packages
RUN apt-get update \
 && apt-get install -y python3-pip git libtinfo-dev libzmq3-dev libcairo2-dev libpango1.0-dev libmagic-dev libblas-dev liblapack-dev

# Install Jupyter notebook
RUN pip3 install -U jupyter
EXPOSE 8888

# vim: nospell
