{-# LANGUAGE GADTs
  , StandaloneDeriving
  , ViewPatterns
  , FlexibleInstances
  , TypeFamilies
  , TypeOperators
  , FlexibleContexts
#-}
module Mussles where

import Data.Array.Accelerate
import Data.Array.Accelerate.LLVM.Native
import qualified Data.Array.Accelerate as DA
import Data.Time.Clock.POSIX

type DataPoint = (Float, Float)

data Scheme = Forward | Backward | Central
differenceScheme = Backward


-- Algal Exchange parameters
aup      =  1.1        -- g/m3  Algal concentration in upper layer  Oosterschelde data
h        =  0.10       -- m     Height of the lower layer  defined
f        =  100.0      -- m3/m3/h  Phichange rate with upper layer  Guestimated

-- Mussel update, growth & mortality parameters
c        =  0.1        -- g/g/h Maximal consumption rate of the mussels  Riisgard 2001
e        =  0.2        -- g/g   Trophic efficiency of mussels  Lamie
dM       =  0.02       -- g/g/h Density dependent mortality rate of the mussels  Calibrated
kM       =  150.0      -- g/m2  Effect of density on mortality  Guestimated

-- Spatial movement parameters
d        =  0.0005     -- m2/h  The diffusion constant describing the movement of mussels
v        =  0.1*60*60  -- m/h   Tidal advection constant(0.1 m/s * 60 sec * 60 min)


phi      =  1000.0    -- Speeding constant, accelerates mussel growth

length_X    = 150      -- Length of the physical landscape
length_Y    = 100      -- Length of the physical landscape

grid_Width, grid_Height :: Exp Float
grid_Width  = 768      -- Size of the 2D grid in X dimension
grid_Height = 512      -- Size of the 2D grid in Y dimension
blockSize   = 32       -- Size of the workgroup with the GPU

endTime  =  180*24/phi -- Total simulation time
numPlots =  180        -- Number of times the figure is updated
dT       =  0.00025    -- Time step

dX = length_X/grid_Width  -- Spatial step size
dY = length_Y/grid_Height -- Spatial step size







simulationKernel :: Stencil3x3 DataPoint -> Exp DataPoint
simulationKernel
     ((_    , top , _    )
     ,(left , curr, right)
     ,(_    , bot,  _    ))
  = lift (m curr + drM * dT * phi, a curr + drA * dT)
  where
    m = DA.fst
    a = DA.snd
    consumption = c * a curr * m curr
    drA = f * (aup - a curr) - consumption / h - v * d_dy a
    drM = e * consumption - dM * m curr * kM / (kM + m curr) + d * d2_dxy2 m

    d2_dxy2 z
      = (z left + z right - 2.0 * z curr) / dX / dX +
        (z top  + z bot   - 2.0 * z curr)/ dY  /dY

    d_dy z
       = case differenceScheme of
           Forward  -> (z top  - z curr)/(dY * 2)
           Backward -> (z curr - z bot) /(dY * 2)
           Central  -> (z top  - z bot) /(dY * 2)

simulationStep ::  Acc (Array DIM2 DataPoint) -> Acc (Array DIM2 DataPoint)
simulationStep arr = stencil simulationKernel clamp arr



hsCalcFrame ::  (Array DIM2 DataPoint) -> Int ->  (Array DIM2 DataPoint)
hsCalcFrame arr steps
  | steps Prelude.<= 0 = arr
  | otherwise  = hsCalcFrame (run1 simulationStep arr) (steps-1)

whileCalcFrame initState
  = awhile cnt next initState
  where
    cnt :: Acc  (Array DIM0 Int, Array DIM2 DataPoint) -> Acc (Scalar Bool)
    cnt st = unit ((afst st) ! (lift Z) DA.>  0)
    next state = lift (cnt', simulationStep arr)
      where arr = asnd state
            cnt' = DA.map (+(-1)) (afst state)

simulate = do
  let dp = run dataPoints
  seq dp (return ())
  t1 <- getCurrentTime
  --let res = run1  calcFrame dp
  --let res = hsCalcFrame dp 1000 -- noOfStepsPerPlot
  let res =  run (whileCalcFrame (lift (unit $ 1000, dp)))
  seq res (return ())
  t2 <- getCurrentTime
  print t1
  print t2

  --print res

  return ()
  where
    -- TODO: handle the borders properly
    simulationStep ::  Acc (Array DIM2 DataPoint) -> Acc (Array DIM2 DataPoint)
    simulationStep arr = stencil simulationKernel clamp arr

    noOfStepsPerPlot :: Int
    noOfStepsPerPlot = Prelude.floor $  endTime / (numPlots * dT  )

    calcFrame :: Acc (Array DIM2 DataPoint) -> Acc (Array DIM2 DataPoint)
    calcFrame dps = foldl1 (.) (Prelude.replicate noOfStepsPerPlot simulationStep) dps

    dataPoints :: Acc (Array DIM2 DataPoint)
    dataPoints = generate (lift $ Z :. DA.floor grid_Height :. DA.floor grid_Width) fn


    fn :: Exp DIM2 -> Exp DataPoint
    fn i =lift (0.0         :: Exp Float
               ,pseudoRand r c :: Exp Float)
           where
              (_ :. r :. c) = unlift i :: (Exp Z :.  Exp Int :. Exp Int)
              pseudoRand i c = DA.fromIntegral (DA.abs ((1 - DA.signum ( (((r+1) * (c+1)) `DA.mod` 527))))) :: Exp Float -- 1 with probablility ca 0.002  TODO: Fix




